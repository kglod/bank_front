import {inject} from 'aurelia-dependency-injection';
import {Project, ProjectItem, CLIOptions, UI} from 'aurelia-cli';

@inject(Project, CLIOptions, UI)
export default class AppGenerator {
  constructor(project, options, ui) {
    this.project = project;
    this.options = options;
    this.ui = ui;
  }

  execute() {
    return this.ui
      .ensureAnswer(this.options.args[0], 'Name of new action and reducer.')
      .then(name => {
        const fileName = this.project.makeFileName(name);
        const functionName = this.project.makeFunctionName(name);
        const className = this.project.makeClassName(name);
        const constName = name.toUpperCase();

        this.project.root.add(
          ProjectItem.text(`flux/actions/${fileName}.js`, this.generateActionSource(name, fileName, functionName, className, constName)),
          ProjectItem.text(`flux/reducers/${fileName}.js`, this.generateReducerSource(name, fileName, functionName, className, constName)),
        );

        const message = `Created ${name} flux.
You can now add those two lines into "src/flux/store.js":
import ${name} from './reducers/${fileName}';
and add ${name} into combineReducers param.
`;
        return this.project.commitChanges()
          .then(() => this.ui.log(message));
      });
  }

  generateActionSource(name, fileName, functionName, className, constName) {
    return `import { getHttp } from 'utils/fetch';
import { ${constName}_URL } from 'conf/urls';


const types = {
  START_FETCHING_${constName}: 'START_FETCHING_${constName}',
  FINISH_FETCHING_${constName}: 'FINISH_FETCHING_${constName}',
  INVALIDATE_${constName}: 'INVALIDATE_${constName}',
};

function _startFetching${className}() {
  return {
    type: types.START_FETCHING_${constName},
  };
}

function _finishFetching${className}(data) {
  return {
    type: types.FINISH_FETCHING_${constName},
    data,
  };
}

function fetch${className}(dispatch) {
  dispatch(_startFetching${className}());

  return getHttp().fetch(${constName}_URL)
    .then(response => response.json())
    .then(json => dispatch(_finishFetching${className}(json)));
}

function _isFetchNeeded(${functionName}State) {
  if (${functionName}State.get('isFetching')) {
    return false;
  } else if (${functionName}State.get('didInvalidate')) {
    return true;
  } else {
    return !${functionName}State.get('fetched');
  }
}

function fetch${className}IfNeeded() {
  return (dispatch, getState) => {
    const ${functionName}State = getState().${functionName};
    if (!_isFetchNeeded(${functionName}State)) {
      return Promise.resolve();
    }

    return fetch${className}(dispatch);
  };
}

function invalidate${className}() {
  return {
    type: types.INVALIDATE_${constName},
  }
}


export { types, fetch${className}IfNeeded, fetch${className}, invalidate${className}, };
`;
  }

  generateReducerSource(name, fileName, functionName, className, constName) {
    return `import { List, Map } from 'immutable';

import { types } from '../actions/${fileName}';


function getInitialState() {
  return new Map({
    isFetching: false,
    didInvalidate: false,
    fetched: false,
    data: new List(),
  });
}


export default function(state, action) {
  if (!state) {
    state = getInitialState();
  }

  switch (action.type) {
    case types.START_FETCHING_${constName}:
      state = state.set('isFetching', true);
      break;
    case types.FINISH_FETCHING_${constName}:
      state = getInitialState().set('data', action.data).set('fetched', true);
      break;
    case types.INVALIDATE_${constName}:
      state = state.set('didInvalidate', true);
      break;
  }

  return state;
}
`;

  }
}

