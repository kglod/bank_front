import {inject} from 'aurelia-dependency-injection';
import {Project, ProjectItem, CLIOptions, UI} from 'aurelia-cli';

@inject(Project, CLIOptions, UI)
export default class AppGenerator {
  constructor(project, options, ui) {
    this.project = project;
    this.options = options;
    this.ui = ui;
  }

  execute() {
    return this.ui
      .ensureAnswer(this.options.args[0], 'Pass a path to new app.')
      .then(path => {
        let name = path.split('/').slice(-1)[0];
        let className = this.project.makeClassName(name);

        this.project.root.add(
          ProjectItem.text(`apps/${path}/${name}.js`, this.generateJSSource(className)),
          ProjectItem.text(`apps/${path}/${name}.html`, this.generateHTMLSource(className))
        );

        return this.project.commitChanges()
          .then(() => this.ui.log(`Created ${className} in ${path} dir.`));
      });
  }

  generateJSSource(className) {
    return `

export class ${className} {

}
`;
  }

  generateHTMLSource(className) {
    return `<template>
<require from="resources/elements/layout/layout.html"></require>

  <layout>
    <div class="row">
      <div class="col s12">
        <h5>${className}</h5>

        <div class="card-panel">
          <p>Hello from <strong>${className}</strong> app!<p>
        </div>

      </div>
    </div>
  </layout>
<template>`;
  }
}

