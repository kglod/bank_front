import i18nConfig from 'conf/i18n';
import validationConfig from 'conf/validation';
import authConfig from 'conf/auth';
import environment from './environment';

//Configure Bluebird Promises.
Promise.config({
  longStackTraces: environment.debug,
  warnings: {
    wForgottenReturn: false
  }
});

export function configure(aurelia) {
  aurelia.use
    .standardConfiguration()
    .plugin('aurelia-materialize-bridge', b => b.useAll())
    .plugin('aurelia-validation', validationConfig)
    .plugin('aurelia-i18n', i18nConfig)
    .plugin('aurelia-auth', authConfig)
    .feature('resources');

  if (environment.debug) {
    aurelia.use.developmentLogging();
  }

  if (environment.testing) {
    aurelia.use.plugin('aurelia-testing');
  }

  aurelia.start().then(() => aurelia.setRoot('apps/app'));
}
