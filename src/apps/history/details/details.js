import StoreSync from 'utils/store-sync';
import { fetchHistoryIfNeeded } from 'flux/actions/history';


export class Details {

  constructor() {
    this.store = StoreSync.init(this, 'history').store;

    this.store.dispatch(fetchHistoryIfNeeded());
  }

  activate(params) {
    this.historyId = params.id;
  }

  get transfer() {
    for (let transfer of this.history.data) {
      if (transfer.id == this.historyId) {
        return transfer;
      }
    }
  }

  downloadRecipt() {
    console.log('downloading...');
  }

}
