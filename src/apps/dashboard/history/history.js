import StoreSync from 'utils/store-sync';
import { fetchHistoryIfNeeded } from 'flux/actions/history';


export class History {

  constructor() {
    this.store = StoreSync.init(this, 'history').store;

    this.store.dispatch(fetchHistoryIfNeeded());
  }

}

