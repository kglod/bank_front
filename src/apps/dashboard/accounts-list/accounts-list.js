import StoreSync from 'utils/store-sync';
import { fetchAccountsIfNeeded } from 'flux/actions/accounts';


export class AccountsList {

  constructor() {
    this.store = StoreSync.init(this, 'accounts').store;

    this.store.dispatch(fetchAccountsIfNeeded());
  }

}

