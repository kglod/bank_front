import { FetchConfig, AuthorizeStep } from 'aurelia-auth';

import { ROUTES } from 'conf/routes';


export class App {

  static inject = [FetchConfig];

  constructor(fetchConfig) {
    this.fetchConfig = fetchConfig;
  }

  configureRouter(config){
    config.options.pushState = true;
    config.options.root = '/';
    config.addPipelineStep('authorize', AuthorizeStep);
    config.map(ROUTES);
  }

  activate() {
    this.fetchConfig.configure();
  }

}
