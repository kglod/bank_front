import { Router } from 'aurelia-router';

import StoreSync from 'utils/store-sync';
import { fetchAccountsIfNeeded } from 'flux/actions/accounts';
import { invalidateTransfer, sendTransfer } from 'flux/actions/transfer';
import { invalidateAccounts } from 'flux/actions/accounts';
import { invalidateHistory } from 'flux/actions/history';
import { handleServerErrors } from 'utils/forms';


export class Sumup {

  static inject = [Router];

  constructor(router) {
    this.router = router;
    this.store = StoreSync.init(this, 'accounts', 'transfer').store;
    this.store.dispatch(fetchAccountsIfNeeded());

    if (!this.transfer.formFilled) {
      this.router.navigate(this.router.generate('transfer'));
    }
  }

  get account() {
    for (let account of this.accounts.data) {
      if (account.account_number == this.transfer.data.from_account) {
        return account;
      }
    }

    return null;
  }

  submit() {
    this.store.dispatch(sendTransfer())
      .then(() => this._success())
      .catch(handleServerErrors);
  }

  _success() {
    this.store.dispatch(invalidateTransfer());
    this.store.dispatch(invalidateAccounts());
    this.store.dispatch(invalidateHistory());

    this.router.navigate(this.router.generate('history'));
  }

  abort() {
    this.store.dispatch(invalidateTransfer());
    this.router.navigate(this.router.generate('transfer'));
  }

  change() {
    this.router.navigate(this.router.generate('transfer'));
  }

}

