import { Router } from 'aurelia-router';
import { ValidationControllerFactory, ValidationRules } from 'aurelia-validation';
import { MaterializeFormValidationRenderer } from 'aurelia-materialize-bridge';

import StoreSync from 'utils/store-sync';
import { fetchAccountsIfNeeded } from 'flux/actions/accounts';
import { fillForm } from 'flux/actions/transfer';


export class Transfer {

  static inject = [Router, ValidationControllerFactory];

  rules = ValidationRules
    .ensure('from_account')
      .minLength(26)
      .maxLength(32)
      .required()
    .ensure('to_account')
      .minLength(26)
      .maxLength(32)
      .required()
    .ensure('amount')
      .required()
      .satisfiesRule('currency')
    .ensure('title')
      .minLength(4)
      .maxLength(255)
      .required()
    .rules;

  constructor(router, controllerFactory) {
    this.router = router;
    this.controller = controllerFactory.createForCurrentScope();
    this.controller.addRenderer(new MaterializeFormValidationRenderer());

    this.store = StoreSync.init(this, 'accounts', 'transfer').store;
    this.store.dispatch(fetchAccountsIfNeeded());

    if (this.transfer.formFilled) {
      this.form = this.transfer.data;
    }
  }

  submit() {
    this.controller.validate().then(v => {
      if (v.valid) {
        this.store.dispatch(fillForm(this.form));
        this.router.navigateToRoute('transfer:sumup');
      }
    });
  }

}
