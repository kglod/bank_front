import { ValidationControllerFactory, ValidationRules } from 'aurelia-validation';
import { MaterializeFormValidationRenderer } from 'aurelia-materialize-bridge';
import { Router } from 'aurelia-router';

import store from 'flux/store';
import { login } from 'flux/actions/auth';


export class Login {

  static inject = [store, ValidationControllerFactory, Router];

  form = {};

  rules = ValidationRules
    .ensure('accountId')
      .minLength(6)
      .maxLength(255)
      .required()
    .ensure('password')
      .minLength(6)
      .maxLength(255)
      .required()
    .rules;

  constructor(store, controllerFactory, router) {
    this.store = store;
    this.router = router;
    this.controller = controllerFactory.createForCurrentScope();
    this.controller.addRenderer(new MaterializeFormValidationRenderer());
  }

  submit() {
    this.controller.validate().then(v => {
      if (v.valid) {
        this.store.dispatch(login(this.form.accountId, this.form.password))
          .then(() => this.router.navigate(this.router.generate('dashboard')));
      }
    });
  }

}

