import { AmountValueConverter } from './amount';


export class AmountWithCurrencyValueConverter {

  CURRENCY_SYMBOL = 'zł';

  toView(value, inGross = true) {
    if (!value) {
      return '';
    }

    value = new AmountValueConverter().toView(value, inGross);
    return `${value}${this.CURRENCY_SYMBOL}`;
  }

  fromView(value, inGross = true) {
    return new AmountValueConverter().fromView(value.replace(this.CURRENCY_SYMBOL, ''), inGross);
  }
}

