
export class AmountValueConverter {
  toView(value, inGross = true) {
    if (!value) {
      return '';
    }

    const CURRENCY_CODE = 'PLN';
    const options = {
      style: 'currency',
      currency: CURRENCY_CODE,
      minimumFractionDigits: 2,
      maximumFractionDigits: 2,
      currencyDisplay: 'symbol',
      useGrouping: false,
    };

    if (!inGross) {
      value = value * 100;
    }

    value = (value / 100).toLocaleString(undefined, options);
    return value.replace(CURRENCY_CODE, '');
  }

  fromView(value, inGross = true) {
    value = Number.parseFloat(value);
    return inGross ? value * 100 : value;
  }
}

