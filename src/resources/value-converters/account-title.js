

export class AccountTitleValueConverter {

  toView(account) {
    if (!account) {
      return '';
    }

    return `${account.first_name} ${account.last_name}`;
  }
}
