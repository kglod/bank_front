
export class AccountNumberValueConverter {
  toView(number) {
    if (!number) {
      return '';
    }

    const spaceAfter = [2, 6, 10, 14, 18, 22];
    let formattedNumber = '';
    for (let i in number.replace(/\s+/g, '')) {
      if (spaceAfter.indexOf(Number.parseInt(i)) !== -1) {
        formattedNumber += ' ';
      }
      formattedNumber += number[i];
    }

    return formattedNumber.slice(0, 32);
  }

  fromView(number) {
    return number ? number.replace(/\s+/g, '') : '';
  }
}

