
export class DatetimeValueConverter {

  toView(value) {
    if (!value) {
      return '';
    }

    return new Date(value).toLocaleDateString(undefined, {hour: 'numeric', minute: 'numeric', second: 'numeric'});
  }

}
