
export class DateValueConverter {

  toView(value) {
    if (!value) {
      return '';
    }

    return new Date(value).toLocaleDateString();
  }

}
