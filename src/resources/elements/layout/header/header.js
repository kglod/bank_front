import { Router } from 'aurelia-router';

import StoreSync from 'utils/store-sync';
import { logout } from 'flux/actions/auth';


export class Header {

  static inject = [Router];

  constructor(router) {
    this.router = router;

    this.store = StoreSync.init(this, 'auth').store;
  }

  logout() {
    this.store.dispatch(logout());
    this.router.navigate(this.router.generate('login'));
  }

}

