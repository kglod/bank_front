import { ValidationMessageProvider, validationMessages, ValidationRules } from 'aurelia-validation';
import { Container } from 'aurelia-dependency-injection';
import { I18N } from 'aurelia-i18n';


validationMessages['minLength'] = '${$displayName} must be at least ${$config.length} characters.';
validationMessages['maxLength'] = '${$displayName} cannot be longer than ${$config.length} characters.';
validationMessages['minItems'] = '${$displayName} must contain at least ${$config.count} items.';
validationMessages['maxItems'] = '${$displayName} cannot contain more than ${$config.count} items.';


function addCustomRules() {
  ValidationRules.customRule(
    'currency',
    value => {
      if (!value) {
        return true;
      }
      const decimalPortion = value.split('.')[1];
      return !decimalPortion || decimalPortion.length <= 2;
    },
    '${$displayName} is incorrect.'
  );
}


export default function validationConfig() {
  const i18next = Container.instance.get(I18N);

  ValidationMessageProvider.prototype.getMessage = function(key) {
    const originalValue = validationMessages[key];
    const translation = i18next.tr(originalValue);

    return this.parser.parseMessage(translation);
  };
  ValidationMessageProvider.prototype.getDisplayName = function(propertyName, displayName) {
    if (displayName !== null && displayName !== undefined) {
      return displayName;
    }
    return i18next.tr(propertyName);
  };

  addCustomRules();
}

