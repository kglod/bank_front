import { AUTH_URL } from './urls';


const config = {
  providers: {},
  baseUrl: AUTH_URL,
  loginUrl: '/login/',
  authToken: 'JWT',
  loginRoute: 'logowanie',
};


export default function configAuth(baseConfig) {
  baseConfig.configure(config);
}
