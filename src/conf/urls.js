
export const BACKEND_URL = 'http://localhost:8001';
export const AUTH_URL = BACKEND_URL + '/login/';
export const ACCOUNTS_URL = BACKEND_URL + '/accounts/';
export const HISTORY_URL = BACKEND_URL + '/history/';
export const TRANSFER_URL = BACKEND_URL + '/transfer/';
