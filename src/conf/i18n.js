import { I18N } from 'aurelia-i18n';
import Backend from 'i18next-xhr-backend';
import environment from 'environment';


export default function i18nConfig(instance) {
  instance.i18next.use(Backend);

  return instance.setup({
    backend: {
      loadPath: './locales/{{lng}}/{{ns}}.json',
    },
    lng : 'pl',
    attributes : ['t','i18n'],
    fallbackLng : 'pl',
    keySeparator: '~',
    debug : environment.debug,
  });
}

