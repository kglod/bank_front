const ROUTES = [
  {
    route: '',
    moduleId: 'apps/dashboard/dashboard',
    name: 'dashboard',
    title: 'Bank.pl | pulpit',
    auth: true,
  },
  {
    route: 'nowy-przelew',
    moduleId: 'apps/transfer/transfer',
    name: 'transfer',
    title: 'Bank.pl | nowy przelew',
    auth: true,
  },
  {
    route: 'nowy-przelew/podsumowanie',
    moduleId: 'apps/transfer/sumup/sumup',
    name: 'transfer:sumup',
    title: 'Bank.pl | nowy przelew - podsumowanie',
    auth: true,
  },
  {
    route: 'historia',
    moduleId: 'apps/history/history',
    name: 'history',
    title: 'Bank.pl | historia przelewów',
    auth: true,
  },
  {
    route: 'historia/:id',
    moduleId: 'apps/history/details/details',
    name: 'history:details',
    title: 'Bank.pl | historia przelewów',
    auth: true,
  },
  {
    route: 'ustawienia',
    moduleId: 'apps/settings/settings',
    name: 'settings',
    title: 'Bank.pl | ustawienia',
    auth: true,
  },
  {
    route: 'logowanie',
    moduleId: 'apps/auth/login/login',
    name: 'login',
    title: 'Bank.pl | zaloguj się',
  },
]


export { ROUTES }
