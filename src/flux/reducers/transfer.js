import { Map, fromJS } from 'immutable';

import { types } from '../actions/transfer';
import { types as authTypes } from '../actions/auth';


function getInitialState() {
  return new Map({
    isSending: false,
    sent: false,
    formFilled: false,
    data: new Map(),
  });
}


export default function(state, action) {
  if (!state) {
    state = getInitialState();
  }

  switch (action.type) {
    case types.FILL_FORM:
      state = state.set('data', fromJS(action.data)).set('formFilled', true);
      break;
    case types.INVALIDATE_TRANSFER:
      state = getInitialState();
      break;
    case types.START_SENDING_TRANSFER:
      state = state.set('isSending', true).set('sent', false);
      break;
    case types.TRANSFER_SENT:
      state = state.set('isSending', false).set('sent', true);
      break;
    case authTypes.LOGGED_OUT:
      state = getInitialState();
      break;
  }

  return state;
}
