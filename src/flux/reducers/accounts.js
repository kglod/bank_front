import { List, Map } from 'immutable';

import { types } from '../actions/accounts';
import { types as authTypes } from '../actions/auth';


function getInitialState() {
  return new Map({
    isFetching: false,
    didInvalidate: false,
    fetched: false,
    data: new List(),
  });
}


export default function(state, action) {
  if (!state) {
    state = getInitialState();
  }

  switch (action.type) {
    case types.START_FETCHING_ACCOUNTS:
      state = state.set('isFetching', true);
      break;
    case types.FINISH_FETCHING_ACCOUNTS:
      state = getInitialState().set('data', action.data).set('fetched', true);
      break;
    case types.INVALIDATE_ACCOUNTS:
      state = state.set('didInvalidate', true);
      break;
    case authTypes.LOGGED_OUT:
      state = getInitialState();
      break;
  }

  return state;
}
