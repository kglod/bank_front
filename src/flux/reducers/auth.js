import { Container } from 'aurelia-dependency-injection';
import { AuthService } from 'aurelia-auth';
import { Map, fromJS } from 'immutable';

import { types } from '../actions/auth';


function getInitialState() {
  const auth = Container.instance.get(AuthService);
  return new Map({
    data: fromJS(auth.getTokenPayload()),
    isAuthenticated: auth.isAuthenticated(),
    authenticating: false,
    token: auth.auth.getToken(),
  });
}


export default function(state, action) {
  if (!state) {
    state = getInitialState();
  }

  switch (action.type) {
    case types.START_LOGGING_IN:
      state = new Map({
        data: new Map(),
        isAuthenticated: false,
        authenticating: true,
      });
      break;
    case types.LOGGED_IN:
      state = getInitialState();
      break;
    case types.LOGGED_OUT:
      state = new Map({
        data: new Map(),
        isAuthenticated: false,
        authenticating: false,
      });
      break;
  }

  return state;

}
