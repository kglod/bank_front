import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import thunkMiddleware from 'redux-thunk'

import auth from './reducers/auth';
import accounts from './reducers/accounts';
import history from './reducers/history';
import transfer from './reducers/transfer';


const reducers = combineReducers({
  auth,
  accounts,
  history,
  transfer,
});

function configureStore() {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  return createStore(
    reducers,
    composeEnhancers(
      applyMiddleware(
        thunkMiddleware,
      ),
    )
  )
}

export default configureStore();

