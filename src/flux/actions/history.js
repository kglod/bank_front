import { getHttp } from 'utils/fetch';
import { HISTORY_URL } from 'conf/urls';


const types = {
  START_FETCHING_HISTORY: 'START_FETCHING_HISTORY',
  FINISH_FETCHING_HISTORY: 'FINISH_FETCHING_HISTORY',
  INVALIDATE_HISTORY: 'INVALIDATE_HISTORY',
};

function _startFetchingHistory() {
  return {
    type: types.START_FETCHING_HISTORY,
  };
}

function _finishFetchingHistory(data) {
  return {
    type: types.FINISH_FETCHING_HISTORY,
    data,
  };
}

function fetchHistory(dispatch) {
  dispatch(_startFetchingHistory());

  return getHttp().fetch(HISTORY_URL)
    .then(response => response.json())
    .then(json => dispatch(_finishFetchingHistory(json)));
}

function _isFetchNeeded(historyState) {
  if (historyState.get('isFetching')) {
    return false;
  } else if (historyState.get('didInvalidate')) {
    return true;
  } else {
    return !historyState.get('fetched');
  }
}

function fetchHistoryIfNeeded() {
  return (dispatch, getState) => {
    const historyState = getState().history;
    if (!_isFetchNeeded(historyState)) {
      return Promise.resolve();
    }

    return fetchHistory(dispatch);
  };
}

function invalidateHistory() {
  return {
    type: types.INVALIDATE_HISTORY,
  }
}


export { types, fetchHistoryIfNeeded, fetchHistory, invalidateHistory, };
