import { json } from 'aurelia-fetch-client';

import { getHttp } from 'utils/fetch';
import { TRANSFER_URL } from 'conf/urls';
import { validateServerResponse } from 'utils/forms';


const types = {
  FILL_FORM: 'FILL_FORM',
  INVALIDATE_TRANSFER: 'INVALIDATE_TRANSFER',
  START_SENDING_TRANSFER: 'START_SENDING_TRANSFER',
  TRANSFER_SENT: 'TRANSFER_SENT',
};

function _startSendingTransfer() {
  return {
    type: types.START_SENDING_TRANSFER,
  };
}

function _transferSent() {
  return {
    type: types.TRANSFER_SENT,
  }
}

function sendTransfer() {
  return (dispatch, getState) => {
    dispatch(_startSendingTransfer());

    const transferState = getState().transfer;
    const transferData = transferState.get('data').toJS();
    transferData.amount *= 100;

    return getHttp().fetch(TRANSFER_URL, {
      method: 'POST',
      body: json(transferData),
    })
      .then(validateServerResponse)
      .then(response => {
        dispatch(_transferSent());
        return response.json();
      });
  };
}

function invalidateTransfer() {
  return {
    type: types.INVALIDATE_TRANSFER,
  }
}

function fillForm(data) {
  return {
    type: types.FILL_FORM,
    data: data,
  }
}


export { types, invalidateTransfer, fillForm, sendTransfer, };
