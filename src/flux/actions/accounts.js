import { getHttp } from 'utils/fetch';
import { ACCOUNTS_URL } from 'conf/urls';


const types = {
  START_FETCHING_ACCOUNTS: 'START_FETCHING_ACCOUNTS',
  FINISH_FETCHING_ACCOUNTS: 'FINISH_FETCHING_ACCOUNTS',
  INVALIDATE_ACCOUNTS: 'INVALIDATE_ACCOUNTS',
};

function _startFetchingAccounts() {
  return {
    type: types.START_FETCHING_ACCOUNTS,
  };
}

function _finishFetchingAccounts(data) {
  return {
    type: types.FINISH_FETCHING_ACCOUNTS,
    data,
  };
}

function fetchAccounts(dispatch) {
  dispatch(_startFetchingAccounts());

  return getHttp().fetch(ACCOUNTS_URL)
    .then(response => response.json())
    .then(json => dispatch(_finishFetchingAccounts(json)));
}

function _isFetchNeeded(accountsState) {
  if (accountsState.get('isFetching')) {
    return false;
  } else if (accountsState.get('didInvalidate')) {
    return true;
  } else {
    return !accountsState.get('fetched');
  }
}

function fetchAccountsIfNeeded() {
  return (dispatch, getState) => {
    const accountsState = getState().accounts;
    if (!_isFetchNeeded(accountsState)) {
      return Promise.resolve();
    }

    return fetchAccounts(dispatch);
  };
}

function invalidateAccounts() {
  return {
    type: types.INVALIDATE_ACCOUNTS,
  }
}


export { types, fetchAccountsIfNeeded, fetchAccounts, invalidateAccounts, };
