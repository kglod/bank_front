import { Container } from 'aurelia-dependency-injection';
import { AuthService } from 'aurelia-auth';


const types = {
  START_LOGGING_IN: 'START_LOGGING_IN',
  LOGGED_IN: 'LOGGED_IN',
  LOGGED_OUT: 'LOGGED_OUT',
};

function _getAuth() {
  return Container.instance.get(AuthService);
}

function _startLogging() {
  return {
    type: types.START_LOGGING_IN,
  };
}

function _loggedIn() {
  return {
    type: types.LOGGED_IN,
  };
}

function _loggedOut() {
  return {
    type: types.LOGGED_OUT,
  };
}

function login(accountId, password) {
  return dispatch => {
    dispatch(_startLogging());
    const userData = {
      username: accountId,
      password: password,
    };

    return _getAuth().login(userData)
      .then(() => dispatch(_loggedIn()));
  };
}

function logout() {
  return dispatch => {
    _getAuth().logout();

    return dispatch(_loggedOut());
  };
}


export { types, login, logout, };
