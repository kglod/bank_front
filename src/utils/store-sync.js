import { Container } from 'aurelia-dependency-injection';
import store from 'flux/store';


export default class StoreSync {

  static init(...args) {
    return new StoreSync(...args);
  }

  constructor(viewModel, ...args) {
    this.viewModel = viewModel;
    this.store = Container.instance.get(store);
    this._syncStateKeys = args;

    this._initSyncedStates();
    this._sync(true);
    this._syncInvoker = this.store.subscribe(() => this._sync());
  }

  detached() {
    if (this._syncInvoker) {
      this._syncInvoker();
    }
  }

  _fillSyncedState(state) {
    for (const stateKey of this._syncStateKeys) {
      this._syncedState[stateKey] = state[stateKey];
    }
  }

  _initSyncedStates() {
    const state = this.store.getState();
    this._syncedState = {};
    this._fillSyncedState(state);
  }

  _isSyncNeeded() {
    const state = this.store.getState();
    for (const stateKey of this._syncStateKeys) {
      if (this._syncedState[stateKey] != state[stateKey]) {
        return true;
      }
    }

    return false;
  }

  _sync(force = false) {
    const state = this.store.getState();
    if (!force && !this._isSyncNeeded()) {
      return;
    }

    for (const stateKey of this._syncStateKeys) {
      this.viewModel[stateKey] = state[stateKey].toJS();
    }

    this._fillSyncedState(state);
  }

}

