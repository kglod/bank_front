import { Container } from 'aurelia-dependency-injection';
import { MdToastService } from 'aurelia-materialize-bridge';

import { ValidationError, ServerError } from 'errors/form';


function handleServerErrors(response) {
  const toast = Container.instance.get(MdToastService);

  response.message.json()
    .then(json => {
      for (let key in json) {
        for (let i in json[key]) {
          toast.show(json[key][i], 4000);
        }
      }
    });
}

function validateServerResponse(response) {
  if (response.status >= 400 && response.status < 500) {
    throw new ValidationError(response);
  } else if (response.status >= 500) {
    throw new ServerError(response);
  }

  return Promise.resolve(response);
}


export { handleServerErrors, validateServerResponse, };
