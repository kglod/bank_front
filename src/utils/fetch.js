import { Container } from 'aurelia-dependency-injection';
import { HttpClient } from 'aurelia-fetch-client';


function getHttp() {
  return Container.instance.get(HttpClient);
}


export { getHttp };
