import BaseError from './base';

export class ValidationError extends BaseError {}
export class ServerError extends BaseError {}
