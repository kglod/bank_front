FROM node:7.4

WORKDIR /app
ADD . /app/

RUN npm install -g aurelia-cli
RUN npm install -g requirejs
RUN npm install
# run optimizer
RUN r.js -o ./rbuild.js

CMD ["au", "run", "--watch"]
